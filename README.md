# Keycloak Tools

This Command Line tool will perform some operations on a running instance of Keycloak.
It requires an admin user in the master realm to gain access.

## Setup

Make sure the following environment variables are set:

```shell
KEYCLOAK_REALM_NAME=my-app
KEYCLOAK_BASE_URL=https://keycloak.my-app.example/auth
```

The environment variables listed below are optional, but give you some additional control:

```shell
# If you use another realm as your master realm, you can change the name
KEYCLOAK_MASTER_REALM_NAME=master 

# The credentials of your admin user on your master realm
KEYCLOAK_ADMIN_USERNAME=admin 
KEYCLOAK_ADMIN_PASSWORD=admin
```

## Features

### Full import

You can perform a full import of realm, clients and users.

First, create a file `data/clients.json` and add your desired client:

```json
[
  {
	"clientId": "your-app-name",
	"enabled": true,
	"publicClient": false,
	"secret": "your-secret",
	"standardFlowEnabled": true,
	"implicitFlowEnabled": false,
	"directAccessGrantsEnabled": true,
	"rootUrl": "http://localhost/",
	"redirectUris": [
	  "http://localhost/oidc_callback"
	],
	"webOrigins": [
	  "*"
	],
	"clientAuthenticatorType": "client-secret"
  }
]
```

Next, create a file `data/users.json` and add all the users that should be preloaded into Keycloak:

```json
[
  {
	"email": "joebloggs@example.com",
	"emailVerified": true,
	"enabled": true,
	"firstName": "Joe",
	"lastName": "Bloggs",
	"username": "joebloggs",
	"credentials": [
	  {
		"temporary": false,
		"type": "password",
		"value": "**********"
	  }
	]
  }
]
```

Now run the following command to import all these things into Keycloak:

```shell
npm start -- full-import
```

### Redirect Uri

To add a redirect URI to an existing client:

```shell
npm start -- redirect-uri add --uri=https://my-app.example.com --client-id=my-app
```

To remove a redirect URI from an existing client:

```shell
npm start -- redirect-uri remove --uri=https://my-app.example.com --client-id=my-app
```

## Usage through Docker (recommended)

We've made it easy to use this tool both manually as well as in Continuous Integration (CI) environments using our Docker image.

You can run any command, prefixed with the docker run command, to use the Docker container.
For example, to perform a full import on your existing Keycloak instance:

```shell
$ docker run \
    -e KEYCLOAK_REALM_NAME=my-app \
    -e KEYCLOAK_BASE_URL=https://keycloak.my-app.example/auth \
    -v myapp/data:/app/data \
    registry.gitlab.com/commonground/huishoudboekje/keycloak-tools/keycloak-tools:1.0.0 \
    npm start -- full-import
```

## Usage in GitLab CI

```yaml
init-keycloak:
  image: registry.gitlab.com/commonground/huishoudboekje/keycloak-tools/keycloak-tools:1.0.0
  variables:
    KEYCLOAK_REALM_NAME: my-app
    KEYCLOAK_BASE_URL: https://keycloak.my-app.example/auth
    DATA_PATH: /builds/$CI_PROJECT_PATH/data
  script:
    - rm -rf /app/data
    - ln -s $DATA_PATH /app/data
    - echo "Initializing Keycloak with the clients and users in the data directory..."
    - npm run start -- full-import
```
